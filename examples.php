<!DOCTYPE html>



<html>



<head>

    <meta charset="utf-8" />

    <meta name="author" content="Scott Gray - FC Online Marketing Inc." />

    <meta name="viewport" content="width=1003, initial-scale=1, maximum-scale=1">



    <title>Custom Martial Arts Websites &amp; Sales Pages | FC Online Marketing</title>

    <meta name="description" content="Check out some of the most advanced websites in the martial arts industry. They are designed to get traffic and turn visitors into customers." />

    <meta name="keywords" content="Websites that sell, martial arts website templates, martial arts marketing online, International website design, martial arts marketing, web site marketing and design, kung fu school sites, Kick boxing webpages, karate center sites" />





    <link rel="stylesheet" type="text/css" href="css/reset.css"/>

    <link rel="stylesheet" type="text/css" href="css/pages.css"/>

    <link rel="stylesheet" type="text/css" href="css/examples.css"/>



    <script type="text/javascript" src="http://www.ilovekickboxing.com/intl_js/jquery.js"></script>

    <script type="text/javascript" src="http://www.ilovekickboxing.com/intl_js/cufon.js"></script>



    <script src="javascript/Myriad-Pro.font.js" type="text/javascript"></script>

    <script src="javascript/Myriad-Pro-Condensed.font.js" type="text/javascript"></script>

    <script src="javascript/Myriad-Pro-Semibold.font.js" type="text/javascript"></script>



    <script type="text/javascript">

    	Cufon.replace('.myriad', { fontFamily: 'Myriad Pro' });

    	Cufon.replace('.condensed', {fontFamily: 'Myriad Pro Condensed'});

    	Cufon.replace('.semibold', { fontFamily: 'Myriad Pro Semibold' });

    </script>



    <script type="text/javascript">

      var _gaq = _gaq || [];

      _gaq.push(['_setAccount', 'UA-29420623-7']);

      _gaq.push(['_trackPageview']);



      (function() {

        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

      })();

    </script>

    <script type="text/javascript">
adroll_adv_id = "IEBR2ZQGSRFY5H3OVLCHAQ";
adroll_pix_id = "7NEMS5UCPFHHTJDVOKUTRQ";
(function () {
var oldonload = window.onload;
window.onload = function(){
__adroll_loaded=true;
var scr = document.createElement("script");
var host = (("https:" == document.location.protocol) ?
"https://s.adroll.com" : "http://a.adroll.com");
scr.setAttribute('async', 'true');
scr.type = "text/javascript";
scr.src = host + "/j/roundtrip.js";
((document.getElementsByTagName('head') || [null])[0] ||
document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
if(oldonload){oldonload()}};
}());
</script>

<!--<script type="text/javascript" src="http://www.fconlinemarketing.com/popdom1213/js.php?popup=1"></script>-->
<!--[if lt IE 9]>
<script src="javascript/html5shiv.js"></script>
<![endif]-->
</head>





<body>



    <header>



        <div id="mast-head">

            <div id="mast-head-content">

                <img src="images/pages-images/fc-logo.png" width="240" height="46" alt="FC Online Marketing" style="margin-top: 20px; float: left;" />

                <div id="mast-head-navigation">

                    <a href="index.php" class="semibold">Home</a>

                    <a href="features.php" class="semibold">Features</a>

                    <a href="examples.php" class="current semibold">Examples</a>

                    <a href="faq.php" class="semibold">FAQ</a>

                    <a href="about.php" class="semibold">About</a>

                    <a href="contact.php" class="semibold">Contact</a>

                    <a href="get-started.php" class="semibold"><em>Get Started Today</em></a>

                    <div style="float: right; height: 88px; line-height: 88px; font-size: 22px; color: #ffffff; margin-left: 30px;" class="myriad">516.543.0041</div>

                </div>

            </div>

        </div>



        <div id="head-panel">

            <div id="head-panel-content">

                <div id="head-panel-copy">

                    <img src="images/examples-images/head-area-h1-examples.png" width="980" height="223" alt="websites that sell" style="margin-top: 15px;"/>

                </div>



                <div id="head-panel-swoop" >

                    <div id="head-tour-link" class="semibold">Scroll down to see our work.</div>

                </div>

            </div>

        </div>



    </header>





        <article>

            <hr style="margin-top: 40px;"/>

            <div class="example-container-outer">



                <div class="example-container-testimonial">

                    <img src="images/index-images/testimonial-17.jpg" width="129" height="130" alt="Brannon Beliso Martial arts instructor" />

                    <h2>

                        &ldquo;In two months we are already closing in on about 50 new trial memberships.&rdquo;

                    </h2>



                  <p class="myriad">FCOM's CEO Michael Parella is a real hands-on school owner and that's one of the many reasons Full Contact Online Marketing is simply amazing. <br/><br/>

                    <strong>My sales site has been up for about two months now and we are closing in on about 50 new trial memberships.</strong> I never got those kind of results from my old website.</p>

                    <p class="signature semibold">

                        Brannon Beliso, San Francisco

                    </p>

                </div>



                <div class="example-container-preview">

                    <img src="images/examples-images/example-site-6.png" width="613" height="484" alt="martial arts school design" />

                    <img src="images/examples-images/example-shadow.png" width="621" height="27" alt="shadow" class="shadow" />

                </div>

            </div>



            <hr style="margin-top: 46px;" />



            <div class="example-container-outer">

                <div class="example-container-preview">

                    <img src="images/examples-images/example-site-10.png" width="613" height="484" alt="Karate school web design" />

                    <img src="images/examples-images/example-shadow.png" width="621" height="27" alt="shadow" class="shadow" />

                </div>



                <div class="example-container-testimonial">

                    <img src="images/index-images/testimonial-20.jpg" width="129" height="130" alt="Bill Clark Karate school owner" />



                    <h2>

                        &ldquo;Our enrollments have doubled within the last 5 months and our classes <br/>are packed.&rdquo;

                    </h2>



                    <p class="myriad">

                       Our websites from Full Contact Online Marketing have had a powerful impact on all of our martial arts programs. Our enrollments have doubled within the last 5 months and our classes are packed. <br/><br/><strong>This was a great investment that I can't recommend highly enough.</strong> Thank you guys for all of the hard work you do. </p>



                    <p class="signature semibold">

                        Bill Clark, Jacksonville

                    </p>

                </div>

            </div>



            <hr style="margin-top: 46px;" />



            <div class="example-container-outer">

                <div class="example-container-testimonial">

                    <img src="images/index-images/testimonial-13.jpg" width="129" height="130" alt="Alan Belcher MMA fighter" />



                    <h2>

                        &ldquo;My lead generation has gone up more than 50%

                        since launching my FCOM site.&rdquo;

                    </h2>



                    <p class="myriad">

                      Immediately since launching my new site,

                      my lead generation went up. In fact, it went up by more

                      than 50% within just a few weeks.<br/><br/> <strong>The

                      site looks awesome, speaks loudly and clearly of all my

                      programs, and most importantly... is boosting business.

                      </strong> Thanks to everyone at FC Online Marketing.

                    </p>



                    <p class="signature semibold">

                        Alan Belcher, Pro MMA Fighter, D'iberville

                    </p>

                </div>



                <div class="example-container-preview">

                    <img src="images/examples-images/example-site-11.png" width="613" height="484" alt="Mixed Martial Arts school site" />

                    <img src="images/examples-images/example-shadow.png" width="621" height="27" alt="shadow" class="shadow" />

                </div>

            </div>



            <hr style="margin-top: 46px;" />



            <div class="example-container-outer">

                <div class="example-container-preview">

                    <img src="images/examples-images/example-site-8.png" width="613" height="484" alt="Kickboxing school web designers" />

                    <img src="images/examples-images/example-shadow.png" width="621" height="27" alt="shadow" class="shadow" />

                </div>



                <div class="example-container-testimonial">

                    <img src="images/index-images/testimonial-19.jpg" width="129" height="130" alt="David Inman Academy owner" />



                    <h2>

                        &ldquo;I've been running a martial arts school for 20+

                        years and I've never been so successful!&rdquo;

                    </h2>



                    <p class="myriad">

                        <strong>I've been running a martial arts school for 20+

                        years and I've never been so successful!</strong>  With

                        the help of FCOM making our website, their fresh

                        insight on the Martial arts business, and how they

                        treat people, has made a huge difference in my

                        business!<br/><br/> My biggest problem now is that

                        I'm running out of space!

                    </p>



                    <p class="signature semibold">

                        David Inman, Las Vegas

                    </p>

                </div>

            </div>







            <hr style="margin-top: 46px;" />



            <div class="example-container-outer">

                <div class="example-container-testimonial">

                    <img src="images/index-images/testimonial-18.jpg" width="129" height="130" alt="Maurice Murphy School owner" />



                    <h2>

                        &ldquo;We get a steady stream of QUALIFIED leads EVERY

                        single week. Thank you!&rdquo;

                    </h2>



                    <p class="myriad">

                        I just wanted to take just a minute to tell you how

                        happy I am that we started using the FCOM web sites.

                        We have had nothing but success since we started using

                        your websites to promote our Martial Arts and Fitness

                        programs. <br/><br/><strong> We get a steady stream of

                        QUALIFIED leads EVERY single week. Thank you!!</strong>

                    </p>



                    <p class="signature semibold">

                        Maurice Murphy, Cummings

                    </p>

                </div>



                <div class="example-container-preview">

                    <img src="images/examples-images/example-site-7.png" width="613" height="484" alt="hap ki do website" />

                    <img src="images/examples-images/example-shadow.png" width="621" height="27" alt="shadow" class="shadow" />

                </div>

            </div>



             <hr style="margin-top: 46px;" />



            <div class="example-container-outer">

                <div class="example-container-preview">

                    <img src="images/examples-images/example-site-4.png" width="613" height="484" alt="boxing kickboxing facility" />

                    <img src="images/examples-images/example-shadow.png" width="621" height="27" alt="shadow" class="shadow" />

                </div>



                <div class="example-container-testimonial">

                    <img src="images/index-images/testimonial-15.jpg" width="129" height="130" alt="Mike Stidham facility owner" />



                    <h2>

                        &ldquo;I could not be happier with my new site! The

                        results have been overwhelming.&rdquo;

                    </h2>



                    <p class="myriad">

                        To the FCOM crew, I could not be happier with my new

                        site! I've been in the business for 20+ years and

                        THOUGHT I knew what people were looking for in a

                        martial arts site. I could not have been further off.

                    </p>



                    <p class="myriad">

                        You really know how to market <br>

                        martial arts. <strong>The results have been

                        overwhelming. </strong>Thank you for all you have done!

                    </p>



                    <p class="signature semibold">

                        Mike Stidham, Draper

                    </p>

                </div>

            </div>



            <hr style="margin-top: 46px;" />



            <div class="example-container-outer">

                <div class="example-container-testimonial">

                    <img src="images/index-images/testimonial-4.jpg" width="129" height="130" alt="David Ross facility owner" />



                    <h2>

                        &ldquo;Thanks to FCOM my monthly billing is over

                        $30,000.. people are <br/>flocking to my school!&rdquo;

                    </h2>



                    <p class="myriad">

                        Thanks to FCOM, my monthly billing is over $30,000 and

                        we are signing up more people than I know what to do

                        with.

                    </p>



                    <p class="myriad"><strong>

                        That's because through my website we're getting 100+

                        web specials PER MONTH.</strong> I know, no one in the

                        industry is getting these kinds of numbers, and that's

                        because there is nothing in our industry like FCOM.

                    </p>



                    <p class="signature semibold">

                        David Ross, New York

                    </p>

                </div>



                <div class="example-container-preview">

                    <img src="images/examples-images/example-site-3.png" width="613" height="484" alt="San Da facility web page" />

                    <img src="images/examples-images/example-shadow.png" width="621" height="27" alt="shadow" class="shadow" />

                </div>

            </div>



            <hr style="margin-top: 46px;" />



            <div class="example-container-outer">

                <div class="example-container-preview">

                    <img src="images/examples-images/example-site-9.png" width="613" height="484" alt="muay thai kickboxing design" />

                    <img src="images/examples-images/example-shadow.png" width="621" height="27" alt="shadow" class="shadow" />

                </div>



                <div class="example-container-testimonial">

                    <img src="images/index-images/testimonial-21.jpg" width="129" height="130" alt="kickboxing marketing clients" />



                    <h2>

                        &ldquo;We attract and convert more members each month

                        than we ever thought possible.&rdquo;

                    </h2>



                    <p class="myriad">

                        FCOM continues to AMAZE! Since they designed and built

                        our new website, we have been out-marketing and

                        out-selling our competition! <br/><br/><strong>We

                        attract and convert more members each month than we

                        ever thought possible. </strong>The site has

                        streamlined our lead gen. so we can now truly focus on

                        serving our clients.

                    </p>



                    <p class="signature semibold">

                        Chuck & Kara Giangreco, Westchester

                    </p>

                </div>

            </div>



            <hr style="margin-top: 46px;" />



            <div class="example-container-outer">

                <div class="example-container-testimonial">

                    <img src="images/index-images/testimonial-11.jpg" width="129" height="130" alt="Bobby Rehka Centre marketing client" />



                    <h2>

                       &ldquo;I've had a lot of leads from the website already

                       and its only been six weeks!&rdquo;

                    </h2>



                    <p class="myriad">

                        Hey gang, I just wanted to thank you for an awesome job

                        on my website... it looks FANTASTIC! You worked toe to

                        toe with me to make sure it was exactly what I wanted.

                    </p>



                    <p class="myriad">

                        <strong>And honestly the website is already working!

                        </strong> I've had a lot of leads from the website

                        already and it's only been six weeks! Thanks again!

                    </p>



                    <p class="signature semibold">

                        Bobby Rekha, Oyster Bay

                    </p>

                </div>



                <div class="example-container-preview">

                    <img src="images/examples-images/example-site-1.png" width="613" height="484" alt="karate center owner" />

                    <img src="images/examples-images/example-shadow.png" width="621" height="27" alt="shadow" class="shadow" />

                </div>

            </div>



            <hr style="margin-top: 46px;" />



            <div class="example-container-outer">

                <div class="example-container-preview">

                    <img src="images/examples-images/example-site-2.png" width="613" height="484" alt="Brazilian Jiu Jitsu MMA site design" />

                    <img src="images/examples-images/example-shadow.png" width="621" height="27" alt="shadow" class="shadow" />

                </div>



                <div class="example-container-testimonial">

                    <img src="images/index-images/testimonial-12.jpg" width="129" height="130" alt="Daniel Sterling MMA Center design client" />



                    <h2>

                        &ldquo;The results that FCOM has given me have truly

                        been <br>life-changing. Thank you.&rdquo;

                    </h2>



                    <p class="myriad">

                        Working with the FCOM Team, I have gotten my business

                        on the right path for success and results have been

                        amazing.

                    </p>



                    <p class="myriad">

                        <strong>We are enrolling more and more people in our

                        academy now then ever before and we aren't even trying.

                        </strong>We are just focusing on our clients and

                        treating people the way they deserve to be treated.

                    </p>



                    <p class="signature semibold">

                        Daniel Sterling, North Attleboro

                    </p>

                </div>

            </div>



            <p>&nbsp;</p>



            <hr style="margin-top: 46px;" />



            <div id="bottom-container">

                 <a href="get-started.php"><img src="images/pages-images/get-started-head.png" width="253" height="67" alt="sign up form" style="position: absolute; right: 35px; top: 35px;" /></a>

            </div>

        </article>



    <footer>

        <?php include("footer.php"); ?>

    </footer>





    <script type="text/javascript"> Cufon.now(); </script>



</body>

</html>