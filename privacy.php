<!DOCTYPE html>







<html>



<head>

    <meta charset="utf-8" />

    <meta name="author" content="Scott Gray - FC Online Marketing Inc." />

    <meta name="viewport" content="width=1003, initial-scale=1, maximum-scale=1">



    <title>Privacy Policy, Terms and Conditions | FC Online Marketing </title>

    <meta name="description" content="We care about your privacy and would never share your information with any third party sites or businesses. Learn more about our privacy policy." />

    <meta name="keywords" content="FC Online Marketing, Martial Arts marketing company, marketing consultation for kickboxing and martial arts school, kung fu schools  karate websites, marketing martial arts, karate school website, martial arts school websites, kung fu marketing online, website for tae kwon do, how to market my martial arts school, FC Online Marketing, international marketing companies" />



    <link rel="stylesheet" type="text/css" href="css/reset.css"/>

    <link rel="stylesheet" type="text/css" href="css/pages.css"/>

    <link rel="stylesheet" type="text/css" href="css/faq.css"/>

    <link rel="stylesheet" type="text/css" href="css/privacy.css"/>



    <script type="text/javascript" src="http://www.ilovekickboxing.com/intl_js/jquery.js"></script>

    <script type="text/javascript" src="http://www.ilovekickboxing.com/intl_js/cufon.js"></script>



    <script src="javascript/Myriad-Pro.font.js" type="text/javascript"></script>

    <script src="javascript/Myriad-Pro-Condensed.font.js" type="text/javascript"></script>

    <script src="javascript/Myriad-Pro-Semibold.font.js" type="text/javascript"></script>



    <script type="text/javascript">

    	Cufon.replace('.myriad', { fontFamily: 'Myriad Pro' });

    	Cufon.replace('.condensed', {fontFamily: 'Myriad Pro Condensed'});

    	Cufon.replace('.semibold', { fontFamily: 'Myriad Pro Semibold' });

    </script>



    <script type="text/javascript">



      var _gaq = _gaq || [];

      _gaq.push(['_setAccount', 'UA-29420623-7']);

      _gaq.push(['_trackPageview']);



      (function() {

        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

      })();



    </script>

    <script type="text/javascript">
adroll_adv_id = "IEBR2ZQGSRFY5H3OVLCHAQ";
adroll_pix_id = "7NEMS5UCPFHHTJDVOKUTRQ";
(function () {
var oldonload = window.onload;
window.onload = function(){
__adroll_loaded=true;
var scr = document.createElement("script");
var host = (("https:" == document.location.protocol) ?
"https://s.adroll.com" : "http://a.adroll.com");
scr.setAttribute('async', 'true');
scr.type = "text/javascript";
scr.src = host + "/j/roundtrip.js";
((document.getElementsByTagName('head') || [null])[0] ||
document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
if(oldonload){oldonload()}};
}());
</script>

<!--<script type="text/javascript" src="http://www.fconlinemarketing.com/popdom1213/js.php?popup=1"></script>-->
<!--[if lt IE 9]>
<script src="javascript/html5shiv.js"></script>
<![endif]-->
</head>







<body>



    <header>

        <div id="mast-head">

            <div id="mast-head-content">

                <img src="images/pages-images/fc-logo.png" width="240" height="46" alt="fc online marketing" style="margin-top: 20px; float: left;" />

                <div id="mast-head-navigation">

                    <a href="index.php" class="semibold">Home</a>

                    <a href="features.php" class="semibold">Features</a>

                    <a href="examples.php" class="semibold">Examples</a>

                    <a href="faq.php" class="semibold">FAQ</a>

                    <a href="about.php" class="semibold">About</a>

                    <a href="contact.php" class="semibold">Contact</a>

                    <a href="get-started.php" class="semibold"><em>Get Started Today</em></a>

                    <div style="float: right; height: 88px; line-height: 88px; font-size: 22px; color: #ffffff; margin-left: 30px;" class="myriad">516.543.0041</div>

                </div>

            </div>

        </div>



        <div id="head-panel">

            <div id="head-panel-content">

                <div id="head-panel-copy">

                    <img src="images/faq-images/head-area-h1-faq.png" width="960" height="223" alt="Have a question of your own" style="margin-top: 15px;"/>

                </div>



                <div id="head-panel-swoop" >

                    <div id="head-tour-link" class="semibold">Privacy Policy</div>

                </div>

            </div>

        </div>

    </header>



        <article>

            <hr style="margin-top: 40px; margin-bottom: 25px;"/>





            <p class="myriad"><strong>W</strong><strong>hat information do we collect? </strong>We collect information from you when you register on our site, place an order, subscribe to our newsletter, respond to a survey or fill out a form. </p>

            <p class="myriad">When ordering or registering on our site, as appropriate, you may be asked to enter your: name, e-mail address, mailing address, phone number or credit card information. You may, however, visit our site anonymously.</p>

            <p class="myriad"><strong>What do we use your information for? </strong>Any of the information we collect from you may be used in one of the following ways: </p>

            <p class="myriad">To personalize your experience (your information helps us to better respond to your individual needs)</p>

            <p class="myriad"> To improve customer service (your information helps us to more effectively respond to your customer service requests and support needs)</p>

            <p class="myriad"> To process transactions</p>

            <p class="myriad">Your information, whether public or private, will not be sold, exchanged, transferred, or given to any other company for any reason whatsoever, without your consent, other than for the express purpose of delivering the purchased product or service requested. </p>

            <p class="myriad"> To administer a contest, promotion, survey or other site feature</p>

            <p class="myriad"> To send periodic emails</p>

            <p class="myriad">The email address you provide for order processing, may be used to send you information and updates pertaining to your order, in addition to receiving occasional company news, updates, related product or service information, etc.</p>

            <p class="myriad"><strong>How do we protect your information? </strong>We implement a variety of security measures to maintain the safety of your personal information when you place an order</p>

            <p class="myriad">We offer the use of a secure server. All supplied sensitive/credit information is transmitted via Secure Socket Layer (SSL) technology and then encrypted into our Payment gateway providers database only to be accessible by those authorized with special access rights to such systems, and are required to keep the information confidential.</p>

            <p class="myriad">After a transaction, your private information (credit cards, social security numbers, financials, etc.) will not be stored on our servers.</p>

            <p class="myriad"><strong>Do we use cookies?</strong> Yes (Cookies are small files that a site or its service provider transfers to your computers hard drive through your Web browser (if you allow) that enables the sites or service providers systems to recognize your browser and capture and remember certain information</p>

            <p class="myriad">We use cookies to help us remember and process the items in your shopping cart and compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future.</p>

            <p class="myriad"><strong>Do we disclose any information to outside parties? </strong>We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information. This does not include trusted third parties who assist us in operating our website, conducting our business, or servicing you, so long as those parties agree to keep this information confidential. We may also release your information when we believe release is appropriate to comply with the law, enforce our site policies, or protect ours or others rights, property, or safety. However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.</p>

            <p class="myriad"><strong>Childrens Online Privacy Protection Act Compliance... </strong>We are in compliance with the requirements of COPPA (Childrens Online Privacy Protection Act), we do not collect any information from anyone under 13 years of age. Our website, products and services are all directed to people who are at least 13 years old or older.</p>

            <p class="myriad"><strong>Your Consent... </strong>By using our site, you consent to our privacy policy.</p>

            <p class="myriad"><strong>Changes to our Privacy Policy...</strong> If we decide to change our privacy policy, we will post those changes on this page, and/or update the Privacy Policy modification date below. </p>

            <p class="myriad">This policy was last modified on 09/23/09</p>

            <p class="myriad"><strong>Contacting Us... </strong>If there are any questions regarding this privacy policy you may contact us using the information below. </p>

            <p class="myriad"><a href="mailto:info@ilovekickboxing.com">info@ilovekickboxing.com</a></p>

            <p  class="myriad" style="line-height:12px; font-size:10px;">Privacy Policy Created by Free Privacy Policy</p>



            <hr style="margin-top: 46px;" />



            <div id="bottom-container">

                 <a href="get-started.php"><img src="images/pages-images/get-started-head.png" width="253" height="67" alt="get started today" style="position: absolute; right: 35px; top: 35px;" /></a>

            </div>

        </article>



    <footer>



    <?php include("footer.php"); ?>



    </footer>



    <script type="text/javascript"> Cufon.now(); </script>



</body>



</html>