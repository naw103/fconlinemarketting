<!DOCTYPE html>



<html>



<head>

    <meta charset="utf-8" />



    <meta name="author" content="Scott Gray - FC Online Marketing Inc." />

    <meta name="viewport" content="width=1003, initial-scale=1, maximum-scale=1">



    <title>Get A Great Website &amp; Powerful Sales Tools | FC Online Marketing </title>

    <meta name="description" content="Our web designs and marketing tools will help fill up your classes.  Find out everything you get when you work with FC Online Marketing." />

    <meta name="keywords" content="Best websites for Martial Arts, SEO dominator for websites,  Email and online marketing, FC Online Marketing, karate school websites, great marketing for Martial Arts websites, Mixed Martial Arts Academy site marketing, how to get more students in my fung fu school, tae kwon do marketing packages" />



    <link rel="stylesheet" type="text/css" href="css/reset.css"/>

    <link rel="stylesheet" type="text/css" href="css/pages.css"/>

    <link rel="stylesheet" type="text/css" href="css/about.css"/>

    <link rel="stylesheet" type="text/css" href="css/get-started.css"/>

    <link rel="stylesheet" type="text/css" href="css/pop-over.css"/>



    <script type="text/javascript" src="//www.ilovekickboxing.com/intl_js/jquery.js"></script>

    <script type="text/javascript" src="//www.ilovekickboxing.com/intl_js/cufon.js"></script>



    <script src="javascript/Myriad-Pro.font.js" type="text/javascript"></script>

    <script src="javascript/Myriad-Pro-Condensed.font.js" type="text/javascript"></script>

    <script src="javascript/Myriad-Pro-Semibold.font.js" type="text/javascript"></script>

    <script src="javascript/journal.font.js" type="text/javascript"></script>

    <script src="javascript/pop-over.js" type="text/javascript"></script>



    <script type="text/javascript">

    	Cufon.replace('.myriad', { fontFamily: 'Myriad Pro' });

    	Cufon.replace('.condensed', {fontFamily: 'Myriad Pro Condensed'});

    	Cufon.replace('.semibold', { fontFamily: 'Myriad Pro Semibold' });

        Cufon.replace('.journal', { fontFamily: 'Journal' });

    </script>



    <script type="text/javascript">

      var _gaq = _gaq || [];

      _gaq.push(['_setAccount', 'UA-29420623-7']);

      _gaq.push(['_trackPageview']);



      (function() {

        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

      })();



    </script>

    <script type="text/javascript">
adroll_adv_id = "IEBR2ZQGSRFY5H3OVLCHAQ";
adroll_pix_id = "7NEMS5UCPFHHTJDVOKUTRQ";
(function () {
var oldonload = window.onload;
window.onload = function(){
__adroll_loaded=true;
var scr = document.createElement("script");
var host = (("https:" == document.location.protocol) ?
"https://s.adroll.com" : "http://a.adroll.com");
scr.setAttribute('async', 'true');
scr.type = "text/javascript";
scr.src = host + "/j/roundtrip.js";
((document.getElementsByTagName('head') || [null])[0] ||
document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
if(oldonload){oldonload()}};
}());
</script>

<!--<script type="text/javascript" src="//www.fconlinemarketing.com/popdom1213/js.php?popup=1"></script>-->
<!--[if lt IE 9]>
<script src="javascript/html5shiv.js"></script>
<![endif]-->
</head>



<body>

    <div id="widget-overlay"></div>

    <div class="box">

        <div id="form">

            <img src="images/pop-over-images/upsell-bottom-drawto.png" width="79" height="57" alt="Get a student grabbing website" style="position: absolute; left: 125px; bottom: 214px; z-index: 0;" />



            <div id="website-checkbox" >

                <img src="images/pop-over-images/checkmark.png" width="23" height="33" alt="check" class="checkmark" />

            </div>



            <div id="upsell-checkbox">

                <img src="images/pop-over-images/checkmark.png" width="23" height="33" alt="check" id="upsell-checkmark" class="checkmark" style="display: none;" />

            </div>



            <img src="images/pop-over-images/upsell-drawto.png" width="33" height="43" alt="email marketing" id="upsell-drawto" />



            <div id="top-website-offer">

                <h2 class="semibold">Website Package</h2>



                <p class="myriad">

                    Our amazing all-inclusive website package to bring you more students, each and every month.

                    Comes loaded with all of the features &amp; bonuses on the previous page.

                </p>

            </div>



            <hr class="offer-seperator" />



            <div id="upsell-header-container">

                <h2 id="upsell-header" class="semibold">Add This Program for Unstoppable Success:</h2>

                <p id="upsell-header-description" class="myriad">

                    Click the box next to the program below to automatically

                    add it to your cart.

                </p>

            </div>



            <div id="upsell-container">

                <img src="images/pop-over-images/email-images.png" width="302" height="241" alt="seo" id="product-image" />



                <div id="upsell-pricing">

                    <img src="images/pop-over-images/upsell-pricing-slash.png" width="52" height="31" alt="consultation" style="position: absolute; top: 6px; right: 18px;" />



                    <h2 class="semibold">

                        Only $129

                        $99/month

                    </h2>



                    <p class="myriad">

                        *Price only available today

                        with purchase of website.

                    </p>

                </div>



                <h2 class="semibold">The Martial Arts E-mail Machine</h2>



                <p class="myriad">

                    <span class="semibold">Get top notch emails every month that convert prospects into

                    students.</span> Includes a perfect blend of powerful content designed

                    to position you as an expert... and sales writing that brings home

                    the enrollments. <span class="semibold">Plus we handle the technical setup, so all you

                    have to do is enjoy the steady stream of students.</span> Completely

                    Done-for-You. Highly profitable.

                </p>



                <div id="highlight-top" >

                    <h2 class="semibold">

                        If you have any of the following programs, you can

                        start profiting from the E-Mail Machine ASAP:

                    </h2>

                </div>



                <ul>

                    <li class="myriad">Kids martial arts (All Styles)</li>

                    <li class="myriad">Adult martial arts (All Styles)</li>

                    <li class="myriad">BJJ, MMA, &amp; Muay Thai</li>

                    <li class="myriad">Krav Maga &amp; Self Defense</li>

                </ul>



                <div id="highlight-bottom" >

                    <h2 class="semibold">

                        Includes:

                    </h2>

                </div>



                <div id="includes-container">

                    <div class="bullet-check myriad">

                        Up to 5 of the above Programs!

                    </div>



                    <div class="bullet-check myriad">

                        8 E-mail Jumpstart Package Per Program <span class="journal blue">$500 Value PER Program!</span>

                    </div>



                    <div class="bullet-check myriad">

                        5 E-mails / Month Per Program <span class="journal blue">$200 Value PER Program!</span>

                    </div>



                    <div class="bullet-check myriad">

                        Installation &amp; Set-up <span class="journal blue" style="position: relative;"><img src="images/pop-over-images/small-slash.png" width="34" height="23" alt="online payment" style="position: absolute; top: 6px; left: 0px;" />$129 FREE with website purchase!</span>

                    </div>







                    <div class="bullet-check myriad">

                        Totally Done-for-You. We handle the technical stuff. We handle the writing.

                    </div>

                </div>

            </div>



            <div id="last-chance-upsell">

                <div id="upsell-checkbox-last">

                    <img src="images/pop-over-images/checkmark.png" width="23" height="33" alt="check" id="upsell-checkmark-last" class="checkmark" style="display: none;" />

                </div>



                <h2 class="semibold">Check this box to instantly add the Email Machine to your cart.</h2>

            </div>



            <hr class="offer-seperator" />



            <div id="form-bottom">

                <h2 style="float: left; font-weight: bold; font-size: 22px; font-weight: bold;width: 400px; text-align: right; color: #000000;" class="semibold">

                    What are you waiting for? Go for it.

                </h2>



                <a href="http://www.1shoppingcart.com/SecureCart/SecureCart.aspx?mid=7F55E777-D7BB-4434-9619-443DB509B12E&gid=18a1c06a7cea2f53119331d4cd7ec66c&afid=1464697" id="cart-link"><img src="images/get-started-images/get-started-button.png" width="229" height="61" alt="get started now" style="float: right; margin-right: 100px;"/></a>

            </div>

        </div>

    </div>



    <header>

        <div id="mast-head">

            <div id="mast-head-content">

                <img src="images/pages-images/fc-logo.png" width="240" height="46" alt="fc online marketing" style="margin-top: 20px; float: left;" />



                <div id="mast-head-navigation">

                    <a href="index.php" class="semibold">Home</a>

                    <a href="features.php" class="semibold">Features</a>

                    <a href="examples.php" class="semibold">Examples</a>

                    <a href="faq.php" class="semibold">FAQ</a>

                    <a href="about.php" class="semibold">About</a>

                    <a href="contact.php" class="semibold">Contact</a>

                    <a href="get-started.php" class="current semibold"><em>Get Started Today</em></a>

                    <div style="float: right; height: 88px; line-height: 88px; font-size: 22px; color: #ffffff; margin-left: 30px;" class="myriad">516.543.0041</div>

                </div>

            </div>

        </div>



        <div id="head-panel">

            <div id="head-panel-content">

                <div id="head-panel-copy">

                    <img src="images/get-started-images/head-area-h1-get-started.png" width="980" height="223" alt="more students fast" style="margin-top: 15px;"/>

                </div>



                <div id="head-panel-swoop">

                    <div id="head-tour-link" class="semibold">More Students. Fast.</div>

                </div>

            </div>

        </div>

    </header>



    <article>

        <hr style="margin-top: 40px;"/>



        <p class="myriad" style="margin-top: 40px; width: 852px;">

            Did we say you were just getting a website? The truth is your website comes packed with more powerful features than you can

            shake a stick at. In short: it's an enrollment-driving, online marketing machine. These truly are the most advanced websites in

            the martial arts industry.

        </p>



        <div id="features-container-outer-header">

            <div id="features-container-head" class="myriad">

                Get an Amazing Website, Plus:

            </div>

        </div>



        <div id="features-container-outer">

            <img src="images/get-started-images/amazing-websites.png" width="390" height="400" alt="amazing website" id="amazing-websites-examples" />



            <div>

                <div id="get-started-list-container">

                    <ul style="width: 234px;">

                        <li class="myriad">Done-for-You Lead Capture</li>

                        <li class="myriad">Email Marketing Integration </li>

                        <li class="myriad">Professional, Custom Layout</li>

                        <li class="myriad">High Res., Stunning Graphics </li>

                        <li class="myriad">Proven Sales Copy (Writing)</li>

                        <li class="myriad">Web Hosting</li>

                    </ul>



                    <ul style="margin-left: 16px;">

                        <li class="myriad">10 Email Addresses</li>

                        <li class="myriad">Unlimited Live Support</li>

                        <li class="myriad">Online Payment Integration</li>

                        <li class="myriad">Social Media Integration</li>

                        <li class="myriad">Search Engine Optimization</li>

                        <li class="myriad">Optional Logo Enhancement</li>

                    </ul>

                </div>



                <h3 class="semibold">And... for a limited time get these bonuses: </h3>



                <div id="limited-time-offer-head"></div>



                <div id="limited-time-offer-container">

                    <div id="bonus-one">

                        <h3 class="myriad">Bonus #1: <span style="color: #515151;" class="semibold">Basic Email Marketing</span></h3>



                        <p class="myriad">

                            Often people enter their email addresses on your site, but don't purchase a trial program. Email

                            marketing reaches out to these prospects later - getting them to buy. With this basic package,

                            you'll get 1 such email for each of your programs. (More advanced package available at checkout).

                        </p>

                    </div>



                    <div id="bonus-two">

                        <h3 class="myriad" style="float: left; width: auto;">Bonus #2: <span style="color: #515151;" class="semibold">SEO Dominator&nbsp;</span></h3><span style="color: #515151; font-size: 35px;margin-top: -15px; float: left;" class="journal">Lite</span>



                        <p class="myriad">

                            To give your website a headstart on Google, Yahoo, Bing &amp; other search engines - your site comes

                            loaded with our Basic SEO Plan. Get backlinks, articles, social bookmarks, and other websites

                            pointing to your site, helping it rank on the major search engines. (Completely Done-for-You).

                        </p>

                    </div>



                    <div id="bonus-three">

                        <h3 class="myriad">Bonus #3: <span style="color: #515151;" class="semibold">30-Minute Marketing Consultation</span></h3>



                        <p class="myriad">

                            In this consultation, we'll go over your current internet marketing plan - and help you tweak it

                            for maximum results. Get exclusive tips, action steps, and advice that you can start implementing

                            ASAP. No internet marketing experience required.

                        </p>

                    </div>



                    <div id="bonus-four">

                        <h3 class="myriad">Bonus #4: <span style="color: #515151;" class="semibold">Marketing Materials to Match Your Site</span></h3>



                        <p class="myriad">

                            Get offline marketing pieces to help boost your site's response. These pieces can be mailed,

                            handed out, and posted in your facility and local venues to drive traffic to your site. After all,

                            the best website in the world is useless without quality traffic.

                        </p>

                    </div>



                    <div id="bonus-five">

                        <h3 class="myriad">Bonus #5: <span style="color: #515151;" class="semibold">

                            1 FREE Month of Member Solution's Event Manager:<br />

                            The Preferred Online Checkout System of Martial Arts Schools</span>

                        </h3>



                        <p class="myriad">

                            We've secured a FREE month of this great checkout system for all of our clients. More schools

                            nationwide use this platform than any other. Of course, you can stick with your current

                            checkout system if you already have one - but we can�t recommend this service enough.

                            After your FREE month it's just $4.99/week: the cost of a cup of Starbucks.

                        </p>

                    </div>

                </div>



                <div id="limited-time-offer-footer"></div>



                <div style="width: 813px;">

                    <div id="limited-time-offer-container-foot">

                        <h3 class="semibold" style="width: 515px;float: left; font-size: 20px;padding-left: 28px; padding-top: 20px;">

                            <br>

                          Your high-powered martial arts website is just a click

                          away. And at these prices, our websites pay for

                      themselves with the first few students you get. Click now to get started. </h3>



                        <div id="get-started-now-container">

                            <h3 class="semibold">

                                $599 Setup Fee,*

                                then $199/month.

                            </h3>



                            <img src="images/get-started-images/get-started-button.png" class="in" width="229" height="61" alt="get started today" />

                        </div>

                    </div>

                </div>

            </div>



            <br style="clear: both;" />

        </div>



        <div id="features-container-outer-footer"></div>



        <div id="disclaimer" class="myriad">

            <p>

                *Many of our clients cover their website fees in online enrollments<br />

                alone. Converting them to contracts is just icing on the cake.

            </p>

        </div>



        <hr style="margin-top: 46px; margin-bottom: 26px;" />

        <!--
        <div class="index-testimonial-container">
            <img src="images/index-images/testimonial-1.jpg" width="128" height="130" alt="Gus Larrea Mixed Martial Arts owner" />
            <h2>
            &ldquo;I got 81 paid kids online enrollements in just 2 months, and converted 75% of them<br /> to full contracts.&rdquo; </h2>
            <h3 class="myriad">Gus Larrea - Larrea Mixed Martial Arts, East Rockaway</h3>
        </div>
        -->
        
        <div class="index-testimonial-container">

            <img src="images/index-images/testimonial-3.jpg" width="128" height="130" alt="Larry Batista Karate School owner" />

            <h2>&ldquo;You've helped me earn a terrific living for both myself and my staff! Thank you.&rdquo;</h2>

            <h3 class="myriad">Larry Batista - Traditional Karate America, Bethpage</h3>

        </div>

        <div class="index-testimonial-container">
            <img src="images/index-images/testimonial-14.jpg" width="128" height="130" alt="James Lee Martial Arts school owner" />
            <h2 class="dark">
                &ldquo;I never pay out of pocket for my site.<br /> The paid online intros cover it for me <br /> each month.&rdquo;
            </h2>



            <h3 class="myriad">James Lee - Evolution Martial Arts, Astoria</h3>

        </div>



        <hr style="margin-top: 46px;" />



        <div id="bottom-container">

            <img src="images/pages-images/get-started-head.png" width="253" height="67" alt="get started now" class="in" style="position: absolute; right: 35px; top: 35px;" />

        </div>

    </article>



    <footer>

        <?php include("footer.php"); ?>

    </footer>



    <script type="text/javascript"> Cufon.now(); </script>

</body>

</html>