<!DOCTYPE html>



<html>



<head>

<meta charset="utf-8" />

<meta name="author" content="Scott Gray - FC Online Marketing Inc." />

<meta name="viewport" content="width=1003, initial-scale=1, maximum-scale=1">



<meta name="description" content="Our Martial Arts websites are designed for one thing: to bring you more students, each and every month.  See what everyone is raving about.">

<meta name="keywords" content="karate websites, marketing martial arts, karate school website, martial arts school websites, kung fu marketing online, website for tae kwon do, how to market my martial arts school, FC Online Marketing, international marketing companies">

<title>Custom Martial Arts Website Designs, Sales Pages & Internet Marketing Services</title>

<link rel="stylesheet" type="text/css" href="css/reset.css"/>

<link rel="stylesheet" type="text/css" href="css/pages.css"/>

<link rel="stylesheet" type="text/css" href="css/index.css"/>



<script type="text/javascript" src="//www.ilovekickboxing.com/intl_js/jquery.js"></script>

<script type="text/javascript" src="//www.ilovekickboxing.com/intl_js/cufon.js"></script>



<script src="javascript/Myriad-Pro.font.js" type="text/javascript"></script>

<script src="javascript/Myriad-Pro-Condensed.font.js" type="text/javascript"></script>

<script src="javascript/Myriad-Pro-Semibold.font.js" type="text/javascript"></script>





<script type="text/javascript">

	Cufon.replace('.myriad', { fontFamily: 'Myriad Pro' });

	Cufon.replace('.condensed', {fontFamily: 'Myriad Pro Condensed'});

	Cufon.replace('.semibold', { fontFamily: 'Myriad Pro Semibold' });

</script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-29420623-7']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script type="text/javascript">
adroll_adv_id = "IEBR2ZQGSRFY5H3OVLCHAQ";
adroll_pix_id = "7NEMS5UCPFHHTJDVOKUTRQ";
(function () {
var oldonload = window.onload;
window.onload = function(){
__adroll_loaded=true;
var scr = document.createElement("script");
var host = (("https:" == document.location.protocol) ?
"https://s.adroll.com" : "http://a.adroll.com");
scr.setAttribute('async', 'true');
scr.type = "text/javascript";
scr.src = host + "/j/roundtrip.js";
((document.getElementsByTagName('head') || [null])[0] ||
document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
if(oldonload){oldonload()}};
}());
</script>

<!--[if lt IE 9]>
<script src="javascript/html5shiv.js"></script>
<![endif]-->

<!--<script type="text/javascript" src="//www.fconlinemarketing.com/popdom1213/js.php?popup=1"></script>-->

</head>



<body>

<div id="fb-root"></div>

<div id="fb-root"></div>

<script>(function(d, s, id) {

  var js, fjs = d.getElementsByTagName(s)[0];

  if (d.getElementById(id)) return;

  js = d.createElement(s); js.id = id;

  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";

  fjs.parentNode.insertBefore(js, fjs);

}(document, 'script', 'facebook-jssdk'));</script>



    <header>

        <div id="mast-head">

            <div id="mast-head-content">

                <img src="images/pages-images/fc-logo.png" width="240" height="46" alt="FC Online Marketing" style="margin-top: 20px; float: left;" />

                <div id="mast-head-navigation">

                    <a href="index.php" class="current semibold">Home</a>

                    <a href="features.php" class="semibold">Features</a>

                    <a href="examples.php" class="semibold">Examples</a>

                    <a href="faq.php" class="semibold">FAQ</a>

                    <a href="about.php" class="semibold">About</a>

                    <a href="contact.php" class="semibold">Contact</a>

                    <a href="get-started.php" class="semibold"><em>Get Started Today</em></a>

                    <div style="float: right; height: 88px; line-height: 88px; font-size: 22px; color: #ffffff; margin-left: 30px;" class="myriad">516.543.0041</div>

                </div>

            </div>

        </div>



        <div id="head-panel">

            <div id="head-panel-content">

                <img src="images/index-images/awesome-websites.png" width="566" height="314" alt="custom web designs" style="position: absolute; right: 0px; top: -3px;" />

                <div id="head-panel-copy">

                    <img src="images/index-images/head-area-h1-index.png" width="433" height="195" alt="martial arts websites that sell a lot" style="margin-top: 50px;"/>

                </div>



                <div id="head-panel-swoop">

                    <a href="get-started.php"><img src="images/pages-images/get-started-head.png" width="253" height="67" alt="get started today" style="position: absolute; z-index: 5; top: 10px; left: 10px;" /></a>

                    <div id="head-tour-link" class="semibold">or <a href="features.php" style="text-decoration: underline; border-bottom: 2px solid #17a0f1;">Take a Tour</a></div>

                </div>

            </div>

        </div>



    </header>



        <article>

            <div id="social-networks-outer">

                <div class="social-network-container">

                    <img src="images/index-images/facebook-icon.png" width="58" height="58" alt="facebook" style="padding-left: 6px;" />

                    <center><div class="fb-like" data-href="http://www.fconlinemarketing.com" data-send="false" data-layout="button_count" data-width="90" data-show-faces="false" style="margin-left: 20px;"></div></center>

                </div>

                <div class="social-network-container">

                    <img src="images/index-images/twitter-icon.png" width="58" height="58" alt="twitter" style="padding-left: 6px;" />

                    <div style="margin-left: 17px;"><a href="https://twitter.com/share" class="twitter-share-button" width="90" style="margin-left: 10px;">Tweet</a></div>

                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

                </div>

                <div class="social-network-container">

                    <img src="images/index-images/google-plus-1-icon.png" width="58" height="58" alt="google" style="padding-left: 6px;" />

                    <div style="padding-left: 21px;">

                        <!-- Place this tag where you want the +1 button to render -->

                        <div class="g-plusone" data-size="medium" data-annotation="bubble" data-width="90"></div>



                        <!-- Place this render call where appropriate -->

                        <script type="text/javascript">

                          (function() {

                            var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;

                            po.src = 'https://apis.google.com/js/plusone.js';

                            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);

                          })();

                        </script>

                    </div>

                </div>

            </div>

            <hr style="margin-top: 40px;"/>

            <h1 style="width: 640px; text-align: left; margin-bottom: 26px;" class="myriad">We handle the technology, so you can focus on your school.</h1>

            <div id="callout-panel" style="text-align: center;">

                <div class="callout-widget">

                    <img src="images/index-images/amazing-results.jpg" width="291" height="170" alt="amazing results" />

                    <h2 style="margin-top: 6px; margin-bottom: 10px;" class="myriad">Amazing Results</h2>

                    <p class="myriad">

                        <span style="font-size:16px;">

                            Check out the awe-inspiring results of fellow

                            school owners. In fact, they're so good, we

                            had trouble believing them ourselves. Click

                            now to read their stories.

                        </span>

                </p>

                <a href="examples.php"><img src="images/pages-images/learn-more.png" width="111" height="34" alt="more on results" class="learn-more" /></a>

                </div>



                <div class="callout-widget">

                    <img src="images/index-images/student-grabbing-features.jpg" width="291" height="170" alt="website features" />

                    <h2 style="margin-top: 6px; margin-bottom: 10px;" class="myriad">Student-Grabbing Features</h2>

                    <p class="myriad">

                        <span style="font-size:16px;">

                            With us, you get more than just a website.

                            You get an entire internet-marketing-<br>

                            machine.

                            Your site comes loaded with<br>

                            state-of-the-art features & benefits.

                        </span>

                        <br />

                    </p>

                    <a href="features.php"><img src="images/pages-images/learn-more.png" width="111" height="34" alt="more on site features" class="learn-more" /></a>

                </div>



                <div class="callout-widget">

                    <img src="images/index-images/super-affordable.jpg" width="291" height="170" alt="affordable sites" />

                    <h2 style="margin-top: 6px; margin-bottom: 10px;" class="myriad">Super Affordable</h2>

                    <p class="myriad">

                        <span style="font-size:16px;">

                            You'd be hard-pressed to find websites of this

                            quality, for these prices. In fact, many of our

                            clients never pay out of pocket for their sites.

                        </span>

                        <br />

                    </p>

                    <a href="get-started.php"><img src="images/pages-images/learn-more.png" width="111" height="34" alt="more on price" class="learn-more" /></a>

                </div>

            </div>



            <hr />



            <div id="services-panel">

                <h2 style="margin-top: 37px;" class="myriad">Our Websites Support the Following Services, and More.</h2>

                <br />

                <img src="images/index-images/services-logos.jpg" width="677" height="60" alt="website support logos" />

                <br />

                <br />

            </div>



            <hr />



            <h2 style="margin-top: 39px;" class="myriad">Our Happy Customers</h2>


            <!--
            <div class="index-testimonial-container">

                <img src="images/index-images/testimonial-1.jpg" width="129" height="130" alt="Gus Larrea MMA owner" />

                <h2>&ldquo;I got 81 paid kids online enrollments in just 2 months, and converted 75% of them.&rdquo;</h2>

                <h3 class="myriad">Gus Larrea  - Larrea Mixed Martial Arts, East Rockaway</h3>

            </div>
            -->






            <div class="index-testimonial-container" style="clear: both;">

                <img src="images/index-images/testimonial-2.jpg" width="129" height="130" alt="Wallace Cup School owner" />

                <h2 class="dark">&ldquo;My website is enrolling 30-40 people each month. I can't thank you enough.&rdquo;</h2>

                <h3 class="myriad">Wallace Cupp - American Martial Arts Academy, Houston</h3>

            </div>







            <div class="index-testimonial-container">

                <img src="images/index-images/testimonial-3.jpg" width="128" height="130" alt="Larry Batista Karate School owner" />

                <h2>&ldquo;You've helped me earn a terrific living for both myself and my staff! Thank you.&rdquo;</h2>

                <h3 class="myriad">Larry Batista - Traditional Karate America, Bethpage</h3>

            </div>







            <div class="index-testimonial-container">

                <img src="images/index-images/testimonial-4.jpg" width="128" height="130" alt="David Ross San Da Center Owner" />

                <h2 class="dark">&ldquo;Thanks to my new website my billing check is now over $30,000 each month.&rdquo;</h2>

                <h3 class="myriad">David Ross - NY San Da, New York City</h3>

            </div>







            <div class="index-testimonial-container">

                <img src="images/index-images/testimonial-5.jpg" width="128" height="130" alt="Brett Lectenberg Martial Arts owner" />

                <h2>&ldquo;We get enrollments on our website almost every day now. You guys rock!&rdquo;</h2>

                <h3 class="myriad">Brett Lechtenberg - Personal Mastery Martial Arts, Sandy </h3>

            </div>



            <hr style="margin-top: 46px;" />



            <div id="bottom-container">

                 <a href="get-started.php"><img src="images/pages-images/get-started-head.png" width="253" height="67" alt="start today" style="position: absolute; right: 35px; top: 35px;" /></a>

            </div>



        </article>

    <footer>



    <?php include("footer.php"); ?>



    </footer>

<script type="text/javascript"> Cufon.now(); </script>


<div id="jt_overlay" style="position: absolute; top: 0px; left: 0px; background: #000; background: rgba(0,0,0,.4); width: 100%; height: 500px; z-index: 99;"></div>
<div id="jt_popup" style="background: #FFF; padding-bottom: 20px; border-radius: 2px; width: 800px; height: auto; position: absolute; top: 150px; left: 50%; right: 50%; margin: auto -400px; z-index: 99;">
	<div class="close" style="cursor: pointer; position: absolute; top: 5px; right: 10px; font-size: 20px; color: #000;">X</div>
	<img src="images/maia-mag.png" alt="" style="float: left; margin-top: 20px; margin-right: 10px; margin-left: 0px;"/>
	<p class="" style="margin: 75px 20px 0px; font-size: 26px; color: #000; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif;">
		Responding to the ad in MA Success?<br />
		Use the following code to take $500<br />
		off of the start-up fee for a powerful,<br />
		student-getting website...<br /><br />
		<span style="color: #f1593a;">MAIASPECIAL2015</span>
	</p>
</div>

<script>

$(document).ready(function(){

	var width = $(document).width();
	var height = $(document).height();

	//$('#jt_overlay').width(width);
	//$('#jt_overlay').height(height);

	var elem_height = $('#jt_popup').height();
	var elem_width = $('#jt_popup').width();

	var browser_height = $(window).height();

	var top_pos = (browser_height / 2) - (elem_height / 2);

	//$('#jt_popup').css('top', top_pos);

	$('#jt_overlay, div.close').click(function(){
		$('#jt_overlay').remove();
		$('#jt_popup').remove();
	});




});

</script>
<script>Cufon.refresh();</script>






</body>



</html>