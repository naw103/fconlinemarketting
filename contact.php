<!DOCTYPE html>



<html>



<head>



    <meta charset="utf-8" />



    <meta name="author" content="Scott Gray - FC Online Marketing Inc." />

    <meta name="viewport" content="width=1003, initial-scale=1, maximum-scale=1">



    <title>Contact Us for More Website Information | FC Online Marketing </title>

    <meta name="description" content="Have any questions about marketing, school management and web site design? Contact us to learn how we can turn around your school." />

    <meta name="keywords" content="FC Online marketing, Full Contact Online Marketing, Contact Martial Arts marketing company, Martial Arts industry marketing company in New York, International Advertising Companies for Martial Arts, Contact tae kwon do and kung fu companies" />



    <link rel="stylesheet" type="text/css" href="css/reset.css"/>

    <link rel="stylesheet" type="text/css" href="css/pages.css"/>

    <link rel="stylesheet" type="text/css" href="css/contact.css"/>



    <script type="text/javascript" src="http://www.ilovekickboxing.com/intl_js/jquery.js"></script>

    <script type="text/javascript" src="http://www.ilovekickboxing.com/intl_js/cufon.js"></script>



    <script src="javascript/Myriad-Pro.font.js" type="text/javascript"></script>

    <script src="javascript/Myriad-Pro-Condensed.font.js" type="text/javascript"></script>

    <script src="javascript/Myriad-Pro-Semibold.font.js" type="text/javascript"></script>



    <script type="text/javascript">

    	Cufon.replace('.myriad', { fontFamily: 'Myriad Pro' });

    	Cufon.replace('.condensed', {fontFamily: 'Myriad Pro Condensed'});

    	Cufon.replace('.semibold', { fontFamily: 'Myriad Pro Semibold' });

    </script>



    <script type="text/javascript">

      var _gaq = _gaq || [];

      _gaq.push(['_setAccount', 'UA-29420623-7']);

      _gaq.push(['_trackPageview']);



      (function() {

        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

      })();

    </script>

<script type="text/javascript">
adroll_adv_id = "IEBR2ZQGSRFY5H3OVLCHAQ";
adroll_pix_id = "7NEMS5UCPFHHTJDVOKUTRQ";
(function () {
var oldonload = window.onload;
window.onload = function(){
__adroll_loaded=true;
var scr = document.createElement("script");
var host = (("https:" == document.location.protocol) ?
"https://s.adroll.com" : "http://a.adroll.com");
scr.setAttribute('async', 'true');
scr.type = "text/javascript";
scr.src = host + "/j/roundtrip.js";
((document.getElementsByTagName('head') || [null])[0] ||
document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
if(oldonload){oldonload()}};
}());
</script>    

   <!-- <script type="text/javascript" src="http://www.fconlinemarketing.com/popdom1213/js.php?popup=1"></script> -->

<!--[if lt IE 9]>
<script src="javascript/html5shiv.js"></script>
<![endif]-->    

</head>



<body>

    <header>

        <div id="mast-head">

            <div id="mast-head-content">

                <img src="images/pages-images/fc-logo.png" width="240" height="46" alt="FC Online Marketing" style="margin-top: 20px; float: left;" />



                <div id="mast-head-navigation">

                    <a href="index.php" class="semibold">Home</a>

                    <a href="features.php" class="semibold">Features</a>

                    <a href="examples.php" class="semibold">Examples</a>

                    <a href="faq.php" class="semibold">FAQ</a>

                    <a href="about.php" class="semibold">About</a>

                    <a href="contact.php" class="current semibold">Contact</a>

                    <a href="get-started.php" class="semibold"><em>Get Started Today</em></a>



                    <div style="float: right; height: 88px; line-height: 88px; font-size: 22px; color: #ffffff; margin-left: 30px;" class="myriad">516.543.0041</div>

                </div>

            </div>

        </div>



        <div id="head-panel">

            <div id="head-panel-content">

                <div id="head-panel-copy">

                    <img src="images/contact-images/head-area-h1-contact.png" width="960" height="223" alt="call or write us now" style="margin-top: 15px;"/>

                </div>



                <div id="head-panel-swoop" >

                    <div id="head-tour-link" class="semibold">Contact Us</div>

                </div>

            </div>

        </div>

    </header>



    <article>

        <hr style="margin-top: 40px;"/>

        <div style="width: 860px; margin: auto; position: relative;">

            <p class="myriad" style="margin-top: 40px; margin-bottom: 16px;">

                Have questions? Need help with something? Ready to get started

                but you'd like to speak to someone first?<br />

                Call or write us now using the information below.

            </p>



            <div style="position: absolute; top: 30px; right: 0px; height: 317px; width: 403px;border: 6px solid #e8e8e9">

                <iframe width="403" height="317" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3026.57419647605!2d-73.556821!3d40.661315699999996!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c27ea9aef6d97d%3A0x1a4dbf5fa313fd02!2s1844+Lansdowne+Ave%2C+Merrick%2C+NY+11566!5e0!3m2!1sen!2sus!4v1424983737611"></iframe>

            </div>



            <div id="contact-container-phone" class="myriad"><span class="semibold">Phone</span>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;516.543.0041</div>



            <div id="contact-container-fax" class="myriad"><span class="semibold">Fax</span>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;516.342.5844</div>



            <div id="contact-container-email" class="myriad"><span class="semibold">E-mail</span>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="mailto:info@fconlinemarketing.com">info@fconlinemarketing.com</a></div>



            <div id="contact-container-mail" class="myriad"><span class="semibold" style="float: left;">Mailing Address&nbsp;&nbsp;&nbsp;</span><div style="float: left;margin-left: 10px;">|</div>&nbsp;&nbsp;&nbsp;<div style="display: inline-block;">1844 Lansdowne Ave,<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Merrick, NY 11566</div></div>

        </div>



        <hr style="margin-top: 60px;" />



        <div id="bottom-container">

             <a href="get-started.php"><img src="images/pages-images/get-started-head.png" width="253" height="67" alt="get started today" style="position: absolute; right: 35px; top: 35px;" /></a>

        </div>

    </article>



    <footer>

        <?php include("footer.php"); ?>

    </footer>



    <script type="text/javascript"> Cufon.now(); </script>



</body>

</html>