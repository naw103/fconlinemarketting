<!DOCTYPE html>







<html>



<head>

    <meta charset="utf-8" />

    <meta name="author" content="Scott Gray - FC Online Marketing Inc." />

    <meta name="viewport" content="width=1003, initial-scale=1, maximum-scale=1">



    <title>Web Design &amp; Internet Marketing Questions | FC Online Marketing</title>

    <meta name="description" content="Learn about how FC Online Marketing build

                                    sites, gets them ranked and gets you

                                    clients! Our quick and easy process

                                    dominates the competition." />

    <meta name="keywords" content="Internet marketing for martial arts centers,

                                    martial arts business questions and

                                    answers, FAQ about karate marketing, FC

                                    Online Marketing questions, martial arts

                                    and kickboxing school information.

                                    International marketing companies, internet

                                    marketing for sports and recreation" />



    <link rel="stylesheet" type="text/css" href="css/reset.css"/>

    <link rel="stylesheet" type="text/css" href="css/pages.css"/>

    <link rel="stylesheet" type="text/css" href="css/faq.css"/>



    <script type="text/javascript" src="http://www.ilovekickboxing.com/intl_js/jquery.js"></script>

    <script type="text/javascript" src="http://www.ilovekickboxing.com/intl_js/cufon.js"></script>



    <script src="javascript/Myriad-Pro.font.js" type="text/javascript"></script>

    <script src="javascript/Myriad-Pro-Condensed.font.js" type="text/javascript"></script>

    <script src="javascript/Myriad-Pro-Semibold.font.js" type="text/javascript"></script>



    <script type="text/javascript">

    	Cufon.replace('.myriad', { fontFamily: 'Myriad Pro' });

    	Cufon.replace('.condensed', {fontFamily: 'Myriad Pro Condensed'});

    	Cufon.replace('.semibold', { fontFamily: 'Myriad Pro Semibold' });

    </script>



    <script type="text/javascript">



      var _gaq = _gaq || [];

      _gaq.push(['_setAccount', 'UA-29420623-7']);

      _gaq.push(['_trackPageview']);



      (function() {

        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

      })();



    </script>

    <script type="text/javascript">
adroll_adv_id = "IEBR2ZQGSRFY5H3OVLCHAQ";
adroll_pix_id = "7NEMS5UCPFHHTJDVOKUTRQ";
(function () {
var oldonload = window.onload;
window.onload = function(){
__adroll_loaded=true;
var scr = document.createElement("script");
var host = (("https:" == document.location.protocol) ?
"https://s.adroll.com" : "http://a.adroll.com");
scr.setAttribute('async', 'true');
scr.type = "text/javascript";
scr.src = host + "/j/roundtrip.js";
((document.getElementsByTagName('head') || [null])[0] ||
document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
if(oldonload){oldonload()}};
}());
</script>

<!--<script type="text/javascript" src="http://www.fconlinemarketing.com/popdom1213/js.php?popup=1"></script>-->
<!--[if lt IE 9]>
<script src="javascript/html5shiv.js"></script>
<![endif]-->
</head>







<body>



    <header>

        <div id="mast-head">

            <div id="mast-head-content">

                <img src="images/pages-images/fc-logo.png" width="240" height="46" alt="FC Online Marketing" style="margin-top: 20px; float: left;" />

                <div id="mast-head-navigation">

                    <a href="index.php" class="semibold">Home</a>

                    <a href="features.php" class="semibold">Features</a>

                    <a href="examples.php" class="semibold">Examples</a>

                    <a href="faq.php" class="current semibold">FAQ</a>

                    <a href="about.php" class="semibold">About</a>

                    <a href="contact.php" class="semibold">Contact</a>

                    <a href="get-started.php" class="semibold"><em>Get Started Today</em></a>

                    <div style="float: right; height: 88px; line-height: 88px; font-size: 22px; color: #ffffff; margin-left: 30px;" class="myriad">516.543.0041</div>

                </div>

            </div>

        </div>



        <div id="head-panel">

            <div id="head-panel-content">

                <div id="head-panel-copy">

                    <img src="images/faq-images/head-area-h1-faq.png" width="960" height="223" alt="Frequently Asked Questions" style="margin-top: 15px;"/>

                </div>



                <div id="head-panel-swoop" >

                    <div id="head-tour-link" class="semibold">FAQ</div>

                </div>

            </div>

        </div>

    </header>



        <article>

            <hr style="margin-top: 40px; margin-bottom: 14px;"/>



            <div class="faq-column" style="margin-left: 20px;">

                <h2 class="semibold" style="margin-top: 20px;">1. How long does it take to make my site?</h2>

                <p class="myriad">

                    <img src="images/faq-images/question-1.png" width="54" height="69" alt="how long" />

                    Our CEO Michael Parrella hired a martial arts web

                    company to make his school's site a few years ago...<br />

                    &amp; he hated how long it took to get anything done.</p>



                <p class="myriad">

                    <strong>That's why at FCOM, if you provide us all of the

                    info we need to make your site, it takes just 14-21 days

                    for you to get rockin' and rollin'. </strong>Need changes

                    to your site? Simple ones like schedule and offer changes

                    take just 12-24 hours.

                </p>



                <p class="myriad">

                    Forget waiting weeks to fix a single typo. We've got you

                    covered.

                </p>



                <h2 class="semibold">2. How long do I pay for my site?</h2>

                <p class="myriad">

                    <img src="images/faq-images/question-2.png" width="83" height="65" alt="how long" style="margin-right: 0px;" />

                    Hopefully forever! Our philosophy is this:

                    <br /><br />

                    <strong>If we provide you with a killer site that looks

                    awesome... and makes you a lot of money... why would you

                    want to stop?</strong>

                </p>



                <p class="myriad">

                    Our basic term is a 12-month contract. After that, it goes month-to-

                    month. The good news is our prices are so affordable - that your site

                    pays for itself with the first couple of students you get.

                </p>



                <h2 class="semibold">3. What if I have multiple schools?</h2>



                <p class="myriad">

                    <img src="images/faq-images/question-3.png" width="79" height="61" alt="multiple schools" />

                    <strong>Additional schools cost only $299 to set up, and just

                    $99/month per school.</strong> Select the multiple schools option

            during our checkout process. </p>



                <p class="myriad">

                    (Click here to access the "Get Started Now" page and check out).</p>



                <h2 class="semibold">4. Does this site replace my current site?</h2>



                <p class="myriad">

                    <img src="images/faq-images/question-4.png" width="78" height="59" alt="replace current site" />

                    That depends. If you've owned a domain name for more

                    than a few months (that's your site's URL, such as

                    www.yourwebsite.com) - then our advice is for this site

                    to replace your current one.

                </p>



                <p class="myriad">

                    <strong>Google & other search engines give more credit to older domains. So

                    it only makes sense to put your new 'enrollment-monster' on your old

                    domain.

            </strong></p>



                <p class="myriad">

                    Don't have a domain? Or have a brand new domain? Then it's up to

                    you whether your new site replaces those, or goes up on a

                    completely new domain.

                </p>



                <h2 class="semibold">5. What do I have to do to setup my website?</h2>



                <p class="myriad">

                    <img src="images/faq-images/question-5.png" width="66" height="72" alt="start up website" />

                    <strong>Nothing! We've got all the technical stuff covered.</strong> Once

                    your site is live, you just have to drive visitors to it (and

                    you get our tested & proven blueprint for that, too).

                </p>

            </div>



            <div class="faq-column" style="width: 460px;padding-right: 20px;">

                <h2 class="semibold" style="margin-top: 20px;">6. How do I get the most use out of my new website?</h2>



                <p class="myriad">

                    <img src="images/faq-images/question-6.png" width="59" height="62" alt="most use" />

                    <strong>All sites come with our Market Dominator 'Lite'. </strong>That's

                    a 10-step blueprint showing you exactly what to do to

            get the most use out of your new website. </p>



                <p class="myriad">

                    So no worries - we've got you covered there, too.

                </p>



                <h2 class="semibold">7. What happens when someone signs up for an intro program on my site?</h2>



                <p class="myriad">

                    <img src="images/faq-images/question-7.png" width="77" height="71" alt="sign ups" />

                    <strong>You receive a registration form in your inbox with all

                    their info. </strong>Then, just contact them by phone, email, or

            text and get them in the door! </p>



                <h2 class="semibold">8. What results can I expect?</h2>



                <p class="myriad">

                    <img src="images/faq-images/question-8.png" width="63" height="60" alt="site results" />

                    This depends on a lot of factors. The biggest factor is

                    you.

                    <br /><br />

                    <strong>Think of your website like this: We're handing you a

                    high-powered rifle that's state-of-the-art, and extremely accurate.

                It's up to you to pull the trigger. </strong></p>



                <p class="myriad">

                    If you follow our tested & proven systems for driving people to your

                    new website, you can expect great results. If you just let it sit and

                    collect dust, it's not going to do very much for you.

                </p>



                <h2 class="semibold">9. How do I receive my monthly billing?</h2>



                <p class="myriad">

                    <img src="images/faq-images/question-9.png" width="69" height="69" alt="monthly billing" />

                    That depends on what third-party service you use to

                    collect payments. <strong>The one we're most familiar with (&

                    highly recommend) is Member Solution's Event

                    Manager. </strong>99% of our clients use this service and love it (and you get

            a FREE month with them when you buy a website through us).</p>



                <p class="myriad">

                    With their service, you can choose to receive your billing monthly,

                    bi-monthly, etc.</p>



                <h2 class="semibold">10. Can I use this site to communicate with my current students?</h2>



                <p class="myriad">

                    <img src="images/faq-images/question-10.png" width="72" height="66" alt="current students" />

                    <strong>Our websites are built for one thing: converting

                    prospects into students.</strong> These are not websites to list

                    your upcoming events... post pictures of the last

            birthday party you held... or anything like that. </p>



                <p class="myriad">

                    These sites are enrollment-driving-monsters designed to help your

                    school explode. Plain and simple.</p>



            </div>



            <br style="clear: both;" />



            <hr style="margin-top: 46px;" />



            <div id="bottom-container">

                 <a href="get-started.php"><img src="images/pages-images/get-started-head.png" width="253" height="67" alt="sign up" style="position: absolute; right: 35px; top: 35px;" /></a>

            </div>

        </article>



    <footer>



    <?php include("footer.php"); ?>



    </footer>



    <script type="text/javascript"> Cufon.now(); </script>



</body>



</html>