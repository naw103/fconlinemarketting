<!DOCTYPE html>



<html>



<head>



<meta charset="utf-8" />

<meta name="author" content="Scott Gray - FC Online Marketing Inc." />

<meta name="viewport" content="width=1003, initial-scale=1, maximum-scale=1">



<title>School Website and Marketing Features | FC Online Marketing</title>

<meta name="description" content="Custom designs, advanced SEO, powerful sales pages, built in social media integration and real customer service when you need it." />

<meta name="keywords" content="Martial arts marketing, online marketing for school sites, karate school business marketing, online advertising, martial arts centre web sites, get more kung fu students, karate website feautures, martial arts industry marketing,  web site for tae kwon do academy, marketing agencies, marketing companies" />





<link rel="stylesheet" type="text/css" href="css/reset.css"/>

<link rel="stylesheet" type="text/css" href="css/pages.css"/>

<link rel="stylesheet" type="text/css" href="css/features.css"/>





<script type="text/javascript" src="http://www.ilovekickboxing.com/intl_js/jquery.js"></script>

<script type="text/javascript" src="http://www.ilovekickboxing.com/intl_js/cufon.js"></script>



<script src="javascript/Myriad-Pro.font.js" type="text/javascript"></script>

<script src="javascript/Myriad-Pro-Condensed.font.js" type="text/javascript"></script>

<script src="javascript/Myriad-Pro-Semibold.font.js" type="text/javascript"></script>





<script type="text/javascript">

	Cufon.replace('.myriad', { fontFamily: 'Myriad Pro' });

	Cufon.replace('.condensed', {fontFamily: 'Myriad Pro Condensed'});

	Cufon.replace('.semibold', { fontFamily: 'Myriad Pro Semibold' });

</script>



<script type="text/javascript">



  var _gaq = _gaq || [];

  _gaq.push(['_setAccount', 'UA-29420623-7']);

  _gaq.push(['_trackPageview']);



  (function() {

    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

  })();



</script>

<script type="text/javascript">
adroll_adv_id = "IEBR2ZQGSRFY5H3OVLCHAQ";
adroll_pix_id = "7NEMS5UCPFHHTJDVOKUTRQ";
(function () {
var oldonload = window.onload;
window.onload = function(){
__adroll_loaded=true;
var scr = document.createElement("script");
var host = (("https:" == document.location.protocol) ?
"https://s.adroll.com" : "http://a.adroll.com");
scr.setAttribute('async', 'true');
scr.type = "text/javascript";
scr.src = host + "/j/roundtrip.js";
((document.getElementsByTagName('head') || [null])[0] ||
document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
if(oldonload){oldonload()}};
}());
</script>

<!--<script type="text/javascript" src="http://www.fconlinemarketing.com/popdom1213/js.php?popup=1"></script>-->

<!--[if lt IE 9]>
<script src="javascript/html5shiv.js"></script>
<![endif]-->
</head>



<body>

    <header>

        <div id="mast-head">

            <div id="mast-head-content">

                <img src="images/pages-images/fc-logo.png" width="240" height="46" alt="FC Online Marketing" style="margin-top: 20px; float: left;" />

                <div id="mast-head-navigation">

                    <a href="index.php" class="semibold">Home</a>

                    <a href="features.php" class="current semibold">Features</a>

                    <a href="examples.php" class="semibold">Examples</a>

                    <a href="faq.php" class="semibold">FAQ</a>

                    <a href="about.php" class="semibold">About</a>

                    <a href="contact.php" class="semibold">Contact</a>

                    <a href="get-started.php" class="semibold"><em>Get Started Today</em></a>

                    <div style="float: right; height: 88px; line-height: 88px; font-size: 22px; color: #ffffff; margin-left: 30px;" class="myriad">516.543.0041</div>

                </div>

            </div>

        </div>



        <div id="head-panel">

            <div id="head-panel-content" class="myriad">

                <div id="head-panel-copy">

                    <img src="images/features-images/head-area-h1-features.png" width="980" height="223" alt="Client enrollment" style="margin-top: 15px;"/>

                </div>



                <div id="head-panel-swoop" class="semibold">

                    <div id="head-tour-link" style="">Scroll Down to Learn More...</div>

                </div>

            </div>

        </div>

    </header>



    <article>



        <hr style="margin-top: 40px;"/>



        <div id="portfolio">

            <img src="images/features-images/beautiful-seo-friendly-sites.png" width="968" height="530" alt="SEO friendly sites" />

        </div>



        <hr style="margin-top: 40px; margin-bottom: 30px;"/>



        <div class="feature-container-outer" >

            <div class="feature-container-left myriad" >

                <h2 class="semibold">New students pay for trial memberships - right on your site.</h2>



                <p>

                    You won't ever have to deal with people who

                    can't afford your services, or

                    weren't planning on buying them in the first

                    place. <strong>Our sites pre-qualify people by having them pay

                    for trial memberships online.</strong> That way you already have

                    their billing info should they sign up for a contract.

                    That makes your job of closing contracts all the easier.

                </p>

            </div>



            <div class="feature-container-right"><img src="images/features-images/features-1.jpg" width="380" height="236" alt="martial arts websites" /></div>

        </div>



        <div class="feature-container-outer">

            <div class="feature-container-left">

                <img src="images/features-images/features-2.jpg" width="380" height="236" alt="karate sales sites" />

            </div>



            <div class="feature-container-right myriad">

                <h2 class="semibold">

                    We've tested &amp; tweaked all

                    possible offers &amp; schedules to insure your site

                    will work wonders.

                </h2>



                <p>

                  The offer on a website has been called the single-most

                  important part. Get it wrong and your phones stay

                  silent. Get it right, and they ring off the hook.

                  Lucky for you - we've gone through all of the

                  offers that simply don't work. And we've

                  found all of the ones that do. <strong>In short: your

                  website is a client-closing machine.</strong>

                </p>

            </div>

        </div>



        <div class="feature-container-outer">

            <div class="feature-container-left myriad">

                <h2 class="semibold">

                    High-res graphics that dramatically<br>

                    boost sales.

                </h2>



                <p>

                    Our team of professional graphic designers have one

                    main talent: <strong>Creating stunning graphics that

                    increase sales.</strong>

                </p>



                <p>

                    They employ state-of-the-art tactics that make your

                    site irresistible to your customers' eyes... and guide

                    them to a sale. Clear navigation makes your site easy

                    to explore - and a professional layout makes you

                    instantly stand out from the crowd.<strong> The result:

                    a profit-boosting site you'll be proud to show off.

                    </strong>

                </p>

            </div>



            <div class="feature-container-right">

                <img src="images/features-images/features-3.jpg" width="380" height="236" alt="great web graphics" />

            </div>

        </div>



        <div class="feature-container-outer">

            <div class="feature-container-left">

                <img src="images/features-images/features-4.jpg" width="380" height="236" alt="sales web content" />

            </div>



            <div class="feature-container-right myriad">

                <h2 class="semibold">

                    Writing that effectively sells your programs, 24/7.

                </h2>



                <p>

                    Ever have those days where you're just on fire

                    with closing people? Where it seems like you deliver

                    the perfect pitch to every person who walks through

                    your doors? Our websites do that, every day of the

                    week. <strong>It doesn't matter if it's a Saturday

                    at 2am or a Monday at 3pm - if someone interested in

                    martial arts comes to your site, it'll get the

                    job done.</strong>

                </p>

            </div>

        </div>



        <div class="feature-container-outer">

            <div class="feature-container-left myriad">

                <h2 class="semibold">

                    Customer service with real human beings, who really

                    care.

                </h2>



                <p>

                    One of the biggest things our clients speak out about is our

                    customer service. They love the fact that any day of the week they

                    can pick up the phone, call us, and a real human being picks up

                    and talks to them on the spot. In our world of email, out-sourcing

                    to foreign countries, and robot-driven answering machines,

                    sometimes <strong>it's nice to just talk to a person and get your questions

                    answered immediately.</strong>

                </p>

            </div>



            <div class="feature-container-right">

                <img src="images/features-images/features-5.jpg" width="380" height="236" alt="customer service" />

            </div>

        </div>



        <div class="feature-container-outer">

            <div class="feature-container-left">

                <img src="images/features-images/features-6.jpg" width="380" height="236" alt="social media on site" />

            </div>



            <div class="feature-container-right myriad">

                <h2 class="semibold">Built-in Social Media Integration.</h2>



                <p>

                    <strong> Have a Facebook fan page? Twitter account? Google+? We'll integrate it

                    right into your site. </strong>Your site will also show how many "likes" you

                    have - adding another layer of social proof that shows why your

                    school rocks. People can also share your site with their friends &amp;

                    family through Facebook on your site, too. It's all just a click away.

                    Plus, your Google+ likes actually contribute to better

                    rankings on Google. <strong>Total Social Media

                    domination.</strong>

                </p>

            </div>

        </div>



        <div class="feature-container-outer">

            <div class="feature-container-left myriad">

                <h2 class="semibold">

                    Advanced SEO Tactics to Help You <br>

                    Dominate Google, Yahoo &amp; More.

                </h2>



                <p>

                    We're continually researching and testing various tactics<br>

                    for crushing the search engines. As a result, your site comes<br>

                    loaded with &quot;ninja&quot; methods that make it stand out on <br>

                    Google, Yahoo, Bing &amp; more.<strong> After all, more visitors =<br>

                    more sales.</strong> And you can see how well your site

                    is doing firsthand with monthly Google Analytics

                    reports. These show you everything from how many

                    visitors you have, to how long they stay on your site.

                </p>

            </div>



            <div class="feature-container-right">

                <img src="images/features-images/features-7.jpg" width="380" height="236" alt="visitors to site" />

            </div>

        </div>



        <div class="feature-container-outer">

            <div class="feature-container-left">

                <img src="images/features-images/features-8.jpg" width="380" height="236" alt="email marketing" />

            </div>



            <div class="feature-container-right myriad">

                <h2 class="semibold">Built-in Email Marketing</h2>



                <p>

                    Often, people will enter their info on your site without<br>

                    purchasing a trial membership. In the past, those prospects<br>

                    were gone with the wind. With our basic email marketing<br>

                    package - you get 2 emails/program that are automatically<br>

                    sent to these prospects. <strong>In turn, many of these &quot;lost prospects&quot;<br>

                    become your loyal students.</strong> (Plus: you can upgrade to our<br>

                    advanced email marketing program for even greater<br>

                    conversions).

                </p>

            </div>

        </div>



        <hr style="margin-top: 46px;" />



        <div class="index-testimonial-container">

            <img src="images/index-images/testimonial-6.jpg" width="129" height="130" alt="Pedro Xavier Tae kwon do academy owner" />



            <h2>&ldquo;My classes are growing faster than we can keep up with. Thanks for all you do.&rdquo;</h2>

            <h3 class="myriad">Pedro Xavier - Boston Tae Kwon Do Academy, Boston</h3>

        </div>



        <div class="index-testimonial-container" style="clear: both;">

            <img src="images/index-images/testimonial-9.jpg" width="129" height="130" alt="Paul Holley Martial Arts instructor" />



            <h2 class="dark">&ldquo;I'm getting an average of 5-7 paid online enrollments every week.&rdquo;</h2>

            <h3 class="myriad">Paul Holley - Holley's International Martial Arts, Jackson Heights</h3>

        </div>



        <div class="index-testimonial-container">

            <img src="images/index-images/testimonial-13.jpg" width="128" height="130" alt="Alan Belcher MMA school owner" />

            <h2>&ldquo;I've gotten 50% more leads within a couple weeks of launching my new site. Thanks!&rdquo;</h2>

            <h3 class="myriad">Alan Belcher - Pro MMA Fighter &amp; Martial Arts School Owner,  D'iberville </h3>

        </div>



        <div class="index-testimonial-container">

            <img src="images/index-images/testimonial-8.jpg" width="128" height="130" alt="Chuck Kara Giangreco academy owner" />



            <h2 class="dark">&ldquo;We were already a successful school. Now we're EXTREMELY successful!&rdquo;</h2>

            <h3 class="myriad">Chuck &amp; Kara  Giangreco - Westchester Martial Arts Academy, Westchester</h3>

        </div>



        <hr style="margin-top: 46px;" />



        <div id="feature-list-container">

            <h2 class="semibold">Even More Powerful Features and Benefits</h2>



            <ul>

                <li class="myriad"><strong>Most changes to your site can be made within 12-24 hrs.</strong> (i.e. Schedule changes that you need updated ASAP. Our customers tell us what a breath of fresh air this is after dealing with companies that take<em> weeks</em> to respond to a single email.</li>

                <li class="myriad">When your site goes live, we do some &ldquo;behind the scenes magic&rdquo; so <strong>Google and other search engines notice it - fast.</strong> Every week the search engines change their "rules" for what gets a site ranked. We stay on the pulse of these changes to help get you to the top.</li>

                <li class="myriad">

                    <strong>Done-for-You Lead Capture:</strong> Your site comes included with

                    everything you need to capture emails of anyone who comes to

                    your site. (That way, if they don't sign up right away, you can sell

                    them later).

                </li>

            </ul>



            <ul>

                <li class="myriad">

                    <strong>Get our 10-step blueprint</strong> that shows you how to use your

                    website for maximum results (learn how to market your site to

                    your school and your area for max enrollments).

                    <br>

                    <br>

                </li>



                <li class="myriad">

                    <strong>Done-for-You Website Hosting.</strong> We handle the technical stuff<br>

                    so you can focus on what you do best: running your school. Hosting can get

                    complicated & costly. Let us bear the load.

                    <br><br>

                </li>



                <li class="myriad">

                    <strong>Optional Logo Enhancement. </strong> We'll give your logo a "face lift" to insure it looks sleek, modern & powerful - at no extra charge. Love your current logo? No prob. We won't change a thing.

                </li>

            </ul>

        </div>



        <hr style="margin-top: 46px;" />



        <div id="bottom-container">

             <a href="get-started.php"><img src="images/pages-images/get-started-head.png" width="253" height="67" alt="sign up form" style="position: absolute; right: 35px; top: 35px;" /></a>

        </div>

    </article>



    <footer>

    <?php include("footer.php"); ?>

    </footer>







    <script type="text/javascript"> Cufon.now(); </script>







</body>







</html>