<!DOCTYPE html>







<html>



<head>

<meta charset="utf-8" />

<meta name="author" content="Scott Gray - FC Online Marketing Inc." />

<meta name="viewport" content="width=1003, initial-scale=1, maximum-scale=1">



<title>FC Online Marketing | Martial Arts Internet Marketing &amp; Web Design</title>

<meta name="description" content="Find out how we started, why our marketing strategies work and how we became one of the industry leaders of martial arts web designs, worldwide.   " />

<meta name="keywords" content="fC online marketing, Mike Parrella, Michael Parrella, Daniella Cipitelli Abruzzo,  karate center websites and marketing, martial arts industry consultants, martial arts web site consultants, MMA centres marketing companies, martial arts business, martial arts management and marketing,  online marketing for karate academies" />



<link rel="stylesheet" type="text/css" href="css/reset.css"/>

<link rel="stylesheet" type="text/css" href="css/pages.css"/>

<link rel="stylesheet" type="text/css" href="css/about.css"/>



<script type="text/javascript" src="http://www.ilovekickboxing.com/intl_js/jquery.js"></script>

<script type="text/javascript" src="http://www.ilovekickboxing.com/intl_js/cufon.js"></script>





<script src="javascript/Myriad-Pro.font.js" type="text/javascript"></script>

<script src="javascript/Myriad-Pro-Condensed.font.js" type="text/javascript"></script>

<script src="javascript/Myriad-Pro-Semibold.font.js" type="text/javascript"></script>



<script type="text/javascript">

	Cufon.replace('.myriad', { fontFamily: 'Myriad Pro' });

	Cufon.replace('.condensed', {fontFamily: 'Myriad Pro Condensed'});

	Cufon.replace('.semibold', { fontFamily: 'Myriad Pro Semibold' });

</script>



<script type="text/javascript">



  var _gaq = _gaq || [];

  _gaq.push(['_setAccount', 'UA-29420623-7']);

  _gaq.push(['_trackPageview']);



  (function() {

    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

  })();



</script>

<script type="text/javascript">
adroll_adv_id = "IEBR2ZQGSRFY5H3OVLCHAQ";
adroll_pix_id = "7NEMS5UCPFHHTJDVOKUTRQ";
(function () {
var oldonload = window.onload;
window.onload = function(){
__adroll_loaded=true;
var scr = document.createElement("script");
var host = (("https:" == document.location.protocol) ?
"https://s.adroll.com" : "http://a.adroll.com");
scr.setAttribute('async', 'true');
scr.type = "text/javascript";
scr.src = host + "/j/roundtrip.js";
((document.getElementsByTagName('head') || [null])[0] ||
document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
if(oldonload){oldonload()}};
}());
</script>

<!--[if lt IE 9]>
<script src="javascript/html5shiv.js"></script>
<![endif]-->

<!--<script type="text/javascript" src="http://www.fconlinemarketing.com/popdom1213/js.php?popup=1"></script>-->

</head>







<body>

    <header>

        <div id="mast-head">

            <div id="mast-head-content">

                <img src="images/pages-images/fc-logo.png" width="240" height="46" alt="FC Online Marketing" style="margin-top: 20px; float: left;" />

                <div id="mast-head-navigation">

                    <a href="index.php" class="semibold">Home</a>

                    <a href="features.php" class="semibold">Features</a>

                    <a href="examples.php" class="semibold">Examples</a>

                    <a href="faq.php" class="semibold">FAQ</a>

                    <a href="about.php" class="current semibold">About</a>

                    <a href="contact.php" class="semibold">Contact</a>

                    <a href="get-started.php" class="semibold"><em>Get Started Today</em></a>

                    <div style="float: right; height: 88px; line-height: 88px; font-size: 22px; color: #ffffff; margin-left: 30px;" class="myriad">516.543.0041</div>

                </div>

            </div>

        </div>



        <div id="head-panel">

            <div id="head-panel-content">

                <div id="head-panel-copy">

                    <img src="images/about-images/head-area-h1-about.png" width="980" height="223" alt="the story of Full Contact Marketing" style="margin-top: 15px;"/>

                </div>



                <div id="head-panel-swoop">

                    <div id="head-tour-link" class="semibold">Read Our Story Below.</div>

                </div>

            </div>

        </div>

    </header>



        <article>

            <hr style="margin-top: 40px;"/>

            <p class="myriad" style="margin-top: 40px;">

           <img style="padding-right:20px; padding-bottom:12px;" src="images/about-images/mike.jpg" alt="Mike Parrella CEO" width="179" height="253" align="left">Summer of 2007, Michael Parrella's martial arts school was headed downhill… fast. He needed to do something drastic to get students in, or he'd have to make a choice that no passionate school owner should ever have to make: </p>

            <h2 class="semibold">To close his doors, forever.</h2>



            <p class="myriad">

                But he wasn't going down without a fight. Not after 20+ years

                of teaching. And so he cast aside all of the old, outdated

                marketing tactics that weren't delivering students… and looked

                into growing his school via the internet.

            </p>



            <p class="myriad">

                <strong>First he hired a website company up north that boasted

                great results and great service.</strong> It didn't take long

                to see that they couldn't deliver either. He wasn't getting any

                enrollments from the site - and the company took weeks to

                respond to phone calls and emails. Finally, Michael fired this

                company, and decided he'd have to figure out this whole

                &quot;internet marketing&quot; thing firsthand.

            </p>



            <p class="myriad">

                He began studying feverishly, soaking up every bit of knowledge

                out there.

            </p>



            <p class="myriad">

                <strong>And soon enough, things started to click. </strong>

                After a few months of obsessive learning, testing, and

                tweaking... he pioneered an internet marketing program for his

                school...

            </p>



            <h2 class="semibold">

                <img style="padding-left:20px; padding-bottom:12px;" src="images/about-images/colleen.png" alt="Colleen Tracey President" width="179" height="275" align="right">

                ...and new students started flooding in.

            </h2>



            <p class="myriad">

                He got more students that first month of internet marketing

                than he'd gotten in the entire previous year. Not only was his

                school's fate secured - it was flourishing.

            </p>



            <p class="myriad">

                <strong>Word spread around his close friends who were also

                school owners. He helped them implement the same tactics he

                used - and their schools exploded, too.</strong> Other school

                owners came out of the woodwork asking for the same kind of

                help.

            </p>



            <h2 class="semibold">

                All of them experienced incredible results, fast.

            </h2>



            <p class="myriad">

                Parrella then assembled an amazing team of marketers, designers, and
                school-growing experts to take his ideas to the next level. One of them
                is company president, Colleen Tracey, whose 21+ years in advertising have
                been crucial to the growth of Full Contact Marketing...

            </p>

            <p class="myriad">

                ...an internet marketing company dedicated to one thing:

            </p>



            <p class="myriad">

                <strong>Helping martial arts schools skyrocket... no matter how

                good, bad, or anywhere in-between the economy is.</strong>

            </p>



            <p class="myriad">

                <img style="margin-right:20px; margin-top:20px;" src="images/about-images/theteam.jpg" alt="FC Online Marketing team" width="492" height="357" align="left">

                And because of Parrella's (and so many other school owners')

                horrible experiences with other website companies, FCOM swore

                to do things differently. Like...

            </p>

                       

           

              <p class="myriad">

                <strong>Respond to all customers immediately</strong> whenever

                they have a question or problem...<br/><br/>

               <strong>Stay up-to-date on what kind of marketing works, and

               what doesn't.</strong> That way all customers get a product that

               actually works.<br/><br/>

              <strong> Treat all customers with respect, courtesy, and

              consideration.</strong> In fact, there's an entire staff of

              people committed to these goals:

              </p>

              

            <p class="myriad"><a href="examples.php">Click here</a> to read a few of the stories of schools we've helped. Or <a href="features.php">click here</a> to learn all about our revolutionary websites. </p>



            <hr style="margin-top: 46px;" />



            <div id="bottom-container">

                 <a href="get-started.php"><img src="images/pages-images/get-started-head.png" width="253" height="67" alt="get started today" style="position: absolute; right: 35px; top: 35px;" /></a>

            </div>



        </article>



    <footer>



    <?php include("footer.php"); ?>



    </footer>



    <script type="text/javascript"> Cufon.now(); </script>



</body>



</html>